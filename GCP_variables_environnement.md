# GCP: Variables Environnement

## Créer et Vérifier des variables d'environnement:

Créer une variable d'environnement et remplacez [VOTRE_RÉGION] par la région: 
```
INFRACLASS_REGION=[VOTRE_RÉGION]
INFRACLASS_PROJECT_ID=[VOTRE_ID_DE_PROJET]
```

Effectuer une vérification à l'aide de la commande “echo” :
```
echo $INFRACLASS_REGION
echo $INFRACLASS_PROJECT_ID
```

## Ajouter la variable d'environnement à un fichier

Créer un répertoire et un fichier de config:
```
mkdir infraclass
touch infraclass/config
```

Injecter les variables d'environnement précédentes dans le fichier:
```
echo INFRACLASS_REGION=$INFRACLASS_REGION >> ~/infraclass/config
echo INFRACLASS_PROJECT_ID=$INFRACLASS_PROJECT_ID >> ~/infraclass/config
```

Utiliser la commande “source” pour définir les variables d'environnement:
```
source infraclass/config
```

## Modifier le profil bash pour mettre en place la persistance des variables d'environnement
Editer le fichier caché profile:
```
vi .profile
```

Ajouter la ligne suivante en fin de fichier:
```
source infraclass/config
```
