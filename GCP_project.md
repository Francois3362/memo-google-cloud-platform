# Project

## Infos sur le projet sur lequel je suis connecté
```
gcloud config list
gcloud config list | grep project
```

## Créer une variable d'envirementment contenant l'ID du projet et l'appliquer
```
export PROJECT_1_ID=[votre_ID_projet]
gcloud config set project $PROJECT_1_ID
```
