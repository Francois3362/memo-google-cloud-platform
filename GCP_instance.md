# Instance: 

## Création: 
Exemple de création d'une instance privatenet-us-vm:
```
gcloud compute instances create privatenet-us-vm --zone=us-central1-c --machine-type=f1-micro --subnet=privatesubnet-us --image-family=debian-10 --image-project=debian-cloud --boot-disk-size=10GB --boot-disk-type=pd-standard --boot-disk-device-name=privatenet-us-vm
```

## Lister: 
Pour lister les instances:
```
gcloud compute instances list --sort-by=ZONE
```