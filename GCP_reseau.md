# GCP: Réseau

## Réseau:

### Création:
Création du réseau:
```
gcloud compute networks create privatenet --subnet-mode=custom
```

Création d'un sous-réseau privatesubnet-us coté US:
```
gcloud compute networks subnets create privatesubnet-us --network=privatenet --region=us-central1 --range=172.16.0.0/24
```

Création d'un sous-réseau privatesubnet-eu coté Europe:
```
gcloud compute networks subnets create privatesubnet-eu --network=privatenet --region=europe-west1 --range=172.20.0.0/20
```

### Lister:
Lister les réseaux VPC disponibles: 
```
gcloud compute networks list
```

Lister les sous-réseau VPC disponibles: 
```
gcloud compute networks subnets list --sort-by=NETWORK
```

## Pare-feu

### Création:
Règle de pare-feu pour autoriser icmp, ssh et rdp pour toutes les IPs sur le sous-réseau "privatenet":
```
gcloud compute firewall-rules create privatenet-allow-icmp-ssh-rdp --direction=INGRESS --priority=1000 --network=privatenet --action=ALLOW --rules=icmp,tcp:22,tcp:3389 --source-ranges=0.0.0.0/0
```

### Lister:
Lister les règles du pare-feu:
```
gcloud compute firewall-rules list --sort-by=NETWORK
```
