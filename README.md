# Mémo Google Cloud Platform

Mémo Perso: Réalisé durant ma formation GCP afin de mettre de coté les éléments trèèèèèèèèèèès importants.
Débuter en juillet 2022
## Menu

- [Regions](GCP_regions.md)
- [Variables d'Environnement](GCP_variables_environnement.md)
- [Bucket](GCP_bucket.md)
- [Project](GCP_project.md)
- [Réseau](GCP_reseau.md)
- [Instance](GCP_instance.md)
  
  